var axios = require('axios');
var config = {
  headers: {'X-Auth-Token': '76f1a40563a8484989edd28cd617b294'}
}

// Teams cache by league ID
var leagueTeams = {};

function fetchTeam (url) {
  var URI = url;
  return axios.get(URI,config)
    .then(function (response) {
      return (response.data);
    })
    .catch(function (error) {
      console.log("Error in fetchTeam: " + error);
    });
}

module.exports = {

  fetchLeagues: function (req,res) {
    var URI = 'http://www.football-data.org/v1/competitions/';
    axios.get(URI,config)
      .then(function (response) {
        //console.log(response);
        res.status(200).send(response.data);
      })
      .catch(function (error) {
        console.log("Error: " + error);
        res.sendStatus(500).send(error);
      });

  },

  fetchLeagueTeams: function (req,res) {
    var leagueID = req.params.id;
    if ( !(leagueID in leagueTeams) ) {
      var URI = 'http://api.football-data.org/v1/competitions/'+ leagueID +'/teams';
      axios.get(URI,config)
        .then(function (response) {
          //console.log(response);
          leagueTeams[leagueID] = response.data.teams;
          res.status(200).send(leagueTeams[leagueID]);
        })
        .catch(function (error) {
          console.log("Error: " + error);
          res.sendStatus(500).send(error);
        });
    }
    else {
      res.status(200).send(leagueTeams[leagueID]);
    }
  },

  fetchTeamFixtures: function (req,res) {
    var URI = 'http://api.football-data.org/v1/teams/'+ req.params.id +'/fixtures';
    axios.get(URI,config)
      .then(function (response) {
        //console.log(response);
        res.status(200).send(response.data.fixtures);
      })
      .catch(function (error) {
        console.log("Error: " + error);
        res.sendStatus(500).send(error);
      });
  },

  fetchMatch: function (req,res) {
    var URI = 'http://api.football-data.org/v1/fixtures/'+ req.params.id;
    var responseObj = {};
    axios.get(URI,config)
      .then(function (response) {
        responseObj = response.data;
      })
      .then(function () {
        return fetchTeam(responseObj.fixture._links.homeTeam.href);
      })
      .then(function (data) {
        responseObj['homeTeamObj'] = data;
        return fetchTeam(responseObj.fixture._links.awayTeam.href);
      })
      .then(function (data) {
        responseObj['awayTeamObj'] = data;
        res.status(200).send(responseObj);
      })
      .catch(function (error) {
        console.log("Error in fetchMatch: " + error);
        res.sendStatus(500).send(error);
      });
  },

  fetchLeagueTable: function (req,res) {
    var leagueID = req.params.id;
    var URI = 'http://api.football-data.org/v1/competitions/'+ leagueID +'/leagueTable';
    axios.get(URI,config)
      .then(function (response) {
        //console.log(response);
        res.status(200).send(response.data);
      })
      .catch(function (error) {
        console.log("Error: " + error);
        res.sendStatus(500).send(error);
      });

  },

  fetchRoundFixtures: function (req,res) {
    var leagueID = req.params.id;
    var round = req.query.matchday;
    var URI = 'http://api.football-data.org/v1/competitions/'+ leagueID +'/fixtures?matchday='+ round;
    axios.get(URI,config)
      .then(function (response) {
        //console.log(response);
        res.status(200).send(response.data);
      })
      .catch(function (error) {
        console.log("Error: " + error);
        res.sendStatus(500).send(error);
      });

  },


}
