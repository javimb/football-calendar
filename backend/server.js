var express = require('express');
var app = express();
var controller = require('./controllers/football-data.js');

var port = process.env.PORT || 8090;

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    next();
};
app.use(allowCrossDomain);

var competitionsRouter = express.Router();

competitionsRouter.get('/competitions', controller.fetchLeagues);
competitionsRouter.get('/competitions/:id/teams', controller.fetchLeagueTeams);
competitionsRouter.get('/competitions/:id/leagueTable', controller.fetchLeagueTable);
competitionsRouter.get('/competitions/:id/fixtures', controller.fetchRoundFixtures);

app.use('/api', competitionsRouter);

var teamsRouter = express.Router();

teamsRouter.get('/teams/:id/fixtures', controller.fetchTeamFixtures);

app.use('/api', teamsRouter);

var fixturesRouter = express.Router();

fixturesRouter.get('/fixtures/:id', controller.fetchMatch);

app.use('/api', fixturesRouter);

app.listen(port);
console.log('Listening on port ' + port);
