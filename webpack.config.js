var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './app/index.js',
  output : {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index_bundle.js'
  },
  module: {
    rules: [
      { test: /\.(js)$/, use: 'babel-loader' },
      { test: /\.css$/, use: [ 'style-loader', 'css-loader' ]},
      { test: /\.(jpg|png|svg|gif)$/, use: 'file-loader?name=/static/img/[name].[ext]'},
      { test: /\.json$/, use: 'json-loader'}
    ]
  },
  plugins: [new HtmlWebpackPlugin({
    template: 'app/index.html',
    favicon: 'app/favicon.ico',
  })],
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:8090',
        secure: false
      }
    },
  }
}
