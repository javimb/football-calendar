Football Calendar
===================
Football Calendar with React. Track down your football team matches in a google--like calendar view, league table, fixtures...choose your team between any of the major european leagues, copy match schedule to your google calendar, switch language...

Development Installation
--------------------------
Prerequisites: node, npm  
Football Calendar has two layers: a node backend which communicates with an external api to grab all data (http://www.football-data.org), and a React based frontend.
```shell
$ npm install
$ npm run start
```
Open another shell and:
```shell
$ cd backend
$ npm install
$ node server.js
```
Now you can open your web browser and enter the address: http://localhost:8080

Docker Deployment
------------------
Prerequisites: npm, Docker, docker-compose  
For a quick test, you can start a docker-compose with two containers: one for the backend (node) and another one for the frontend (nginx)
```shell
$ npm install
$ npm run build
$ docker-compose build
$ docker-compose up
```
Now you can open your web browser and enter the address: http://localhost:8080

**Important:** If you are behind a proxy, please set http_proxy and https_proxy variables accordingly in docker-compose.yml file. If not, just leave them blank
