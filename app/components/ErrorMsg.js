var React = require('react');
var PropTypes = require('prop-types');
var Alert = require('react-bootstrap').Alert;

class ErrorMsg extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      alertVisible: false,
    };

    this.handleAlertDismiss = this.handleAlertDismiss.bind(this);

  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.msg) {
      this.setState(function () {
        return {
          alertVisible: true
        }
      });
    }
  }

  render() {
    if (this.state.alertVisible) {
      return (
        <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
          <h4><strong>{this.props.msg.title}</strong></h4>
            <p>{this.props.msg.text}</p>
        </Alert>
      );
    } else {
      return(<div></div>);
    }
  }

  handleAlertDismiss() {
    this.setState({alertVisible: false});
    this.props.onDismiss.call(null)
  }

}

ErrorMsg.propTypes = {
  msg: PropTypes.object,
  onDismiss: PropTypes.func.isRequired,
}

module.exports = ErrorMsg;
