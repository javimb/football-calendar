var React = require('react');
var Grid = require('react-bootstrap').Grid;
var Navbar = require('react-bootstrap').Navbar;
var Nav = require('react-bootstrap').Nav;
var NavItem = require('react-bootstrap').NavItem;
var Image = require('react-bootstrap').Image;
var Team = require('./Team');
var intl = require('react-intl-universal');

// locale data
const locales = {
  "es-ES": require('../../static/locales/es-ES.json'),
  "es": require('../../static/locales/es-ES.json'),
  "en-GB": require('../../static/locales/en-GB.json'),
  "en": require('../../static/locales/en-GB.json'),
};

var espFlag = require('../../static/img/esp.png');
var UKFlag = require('../../static/img/gbr.png');
var logo = require('../../static/img/logo-small.png');

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      initDone: false,
      currentLocale: 'en-GB'
    };
    this.selectLocale = this.selectLocale.bind(this);
  }

  componentDidMount() {
    var localeFromBrowser = navigator.language || navigator.userLanguage;
    if (localeFromBrowser) {
      this.state = {
        currentLocale: localeFromBrowser
      };
    }
    this.loadLocales();
  }

  loadLocales() {
    // init method will load CLDR locale data according to currentLocale
    // react-intl-universal is singleton, so you should init it only once in your app
    intl.init({
      currentLocale: this.state.currentLocale,
      locales,
    })
    .then(function() {
      // After loading CLDR locale data, start to render
	    this.setState(function() {
        return {
          initDone: true
        }
      });
    }.bind(this));
  }

  selectLocale(e) {
    this.state = {
      initDone: false,
      currentLocale: e.target.name
    };
    this.loadLocales();
  }

  render() {
    return (
      <div>
        <Navbar inverse staticTop >
          <Navbar.Header>
              <Image src={logo} responsive className="pull-left" />
              <Navbar.Brand>
                <a href="#"><i>&nbsp;Football Calendar</i></a>
              </Navbar.Brand>
          </Navbar.Header>
          <Nav pullRight>
            <NavItem eventKey={1}>
              <img onClick={this.selectLocale} name="en-GB" className="langFlag" src={UKFlag} alt={intl.get('LANG-ENG')} height="22" width="22"/>
            </NavItem>
            <NavItem eventKey={2}>
              <img onClick={this.selectLocale} name="es-ES" className="langFlag" src={espFlag} alt={intl.get('LANG-ESP')} height="22" width="22"/>
            </NavItem>
          </Nav>
        </Navbar>
        <Grid>
            <Team lang={this.state.currentLocale} />
        </Grid>
      </div>
    )
  }
}

module.exports = App;
