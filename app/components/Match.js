var React = require('react');
var PropTypes = require('prop-types');
var Modal = require('react-bootstrap').Modal;
var Button = require('react-bootstrap').Button;
var Col = require('react-bootstrap').Col;
var Row = require('react-bootstrap').Row;
var api = require('../utils/api');
var intl = require('react-intl-universal');

var dateFmtOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };


class MatchDetails extends React.Component {

  render() {
    return (
        <div>
          <Row className="show-grid">
            <Col xs={6}>
              <img className={"crest-small center-block"} src={this.props.matchDetails.homeTeamObj.crestUrl}/>
            </Col>
            <Col xs={6}>
              <img className={"crest-small center-block"} src={this.props.matchDetails.awayTeamObj.crestUrl}/>
            </Col>
          </Row>
          { !this.props.match.result ?
            (<div>
              <Row className="show-grid">
                <Col xs={12}>
                    <br />
                    <h4 className="text-center">{this.props.match.start.toLocaleDateString(this.props.lang, dateFmtOptions)}</h4>
                </Col>
              </Row>
              <Row className="show-grid">
                <Col xs={12}>
                    <h2 className="text-center"><strong>{this.props.match.schedule}</strong></h2>
                </Col>
              </Row>
              { this.props.match.status==="SCHEDULED" &&
                <Row className="show-grid">
                  <Col xs={12}>
                    <a href={"http://www.google.com/calendar/event?"+
                      "action=TEMPLATE"+
                      "&text="+this.props.match.title+
                      "&dates="+this.props.match.calendar+
                      "&details="+this.props.match.title+
                      "&location="+this.props.match.title.split('-')[0]+" Stadium"}
                      target="_blank" rel="nofollow"><p className="text-center">{intl.get('ADD-CALENDAR')}</p></a>
                  </Col>
                </Row>
              }
            </div>)
            :
            (<div>
              <Row className="show-grid">
                <Col xs={6}>
                  <h1 className="text-center"><strong>{this.props.match.result.split(" - ")[0]}</strong></h1>
                </Col>
                <Col xs={6}>
                  <h1 className="text-center"><strong>{this.props.match.result.split(" - ")[1]}</strong></h1>
                </Col>
              </Row>
            </div>)
          }
        </div>
    );
  }

}

MatchDetails.propTypes = {
  matchDetails: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  lang: PropTypes.string.isRequired
}

class Match extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      matchDetails: null
    };
    this.getMatchDetails = this.getMatchDetails.bind(this);
    this.close = this.close.bind(this);
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.match) {
      this.getMatchDetails(nextProps.match.link);
    }
  }

  getMatchDetails(url) {
    api.fecthMatch(url)
      .then(function (match) {
        this.setState(function () {
          return {
            matchDetails: match
          }
        });
      }.bind(this))
      .catch(function (error) {
        console.log("Error: " + error);
        var errorMsg = {};
        errorMsg['title']= '' + error;
        errorMsg['text']=intl.get('ERR-MATCH') + intl.get('ERR-CHECK');
        this.props.onError.call(null,errorMsg);
        this.close();
      }.bind(this));
  }

  close() {
    this.setState(function () {
      return {
        matchDetails: null
      }
    });
    this.props.onClose.call(null);
  }

  render() {
    return (
      <div>
        { this.props.match &&
          <Modal
            show={this.props.show}
            onHide={this.close}
          >
            <Modal.Header closeButton>
              <Modal.Title>{this.props.match.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              { this.state.matchDetails ? (
                <MatchDetails
                  matchDetails={this.state.matchDetails}
                  match={this.props.match}
                  lang={this.props.lang}
                /> )
                : <div><br /><div className="spinner"></div><br /></div>
              }
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.close}>{intl.get('CLOSE')}</Button>
            </Modal.Footer>
          </Modal>
        }
      </div>
    );
  }
}

Match.propTypes = {
  match: PropTypes.object,
  show: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  lang: PropTypes.string.isRequired,
}

module.exports = Match;
