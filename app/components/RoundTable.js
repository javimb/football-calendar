var React = require('react');
var PropTypes = require('prop-types');
var api = require('../utils/api');
var intl = require('react-intl-universal');
var Table = require('react-bootstrap').Table;
var Pager = require('react-bootstrap').Pager;

class RoundTable extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      fixtures: null,
      offset: 0
    };

    this.handleFixturesPager = this.handleFixturesPager.bind(this);
  }

  componentDidMount () {
    if (this.props.league && this.props.matchday) {
      this.getRoundFixtures(this.props.league,this.props.matchday);
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.league && nextProps.matchday) {
      if (this.props.league!=nextProps.league ||
          this.props.matchday!=nextProps.matchday) {
        this.getRoundFixtures(nextProps.league,nextProps.matchday);
      }
    }
    else {
      this.setState(function () {
        return {
          fixtures: null
        }
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.offset!=this.state.offset) {
      this.getRoundFixtures(this.props.league,this.props.matchday+this.state.offset);
    }
  }

  getRoundFixtures(leagueID,matchday) {

    this.setState(function () {
      return {
        fixtures: null
      }
    });

    api.fetchRoundFixtures(leagueID,matchday)
      .then(function (fixtures) {
        this.setState(function () {
          return {
            fixtures: fixtures
          }
        });
      }.bind(this))
      .catch(function (error) {
        console.log("Error: " + error);
        var errorMsg = {};
        errorMsg['title']= '' + error;
        errorMsg['text']=intl.get('ERR-ROUND') + intl.get('ERR-CHECK');
        this.props.onError.call(null,errorMsg);
      }.bind(this));

  }

  handleFixturesPager (offset,event) {
    //console.log('offset: '+offset);
    this.setState(function () {
      return {
        offset: this.state.offset + offset
      }
    });
  }

  render() {
    var dateFmtOptions = { month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' };
    var minRound = 1;
    var maxRound = (this.state.fixtures)?((this.state.fixtures.fixtures.length*2)-1)*2:0;
    return (
      <Table responsive condensed hover>
        <thead>
          <tr>
            { this.props.matchday &&
              <th colSpan="2">
                <Pager>
                  { this.props.matchday + this.state.offset > minRound &&
                    <Pager.Item
                      previous
                      href="#"
                      onClick={this.handleFixturesPager.bind(null,-1)}>
                      &#060;
                    </Pager.Item>
                  }
                  {intl.get('MATCHDAY')} {this.props.matchday + this.state.offset}
                  { this.props.matchday + this.state.offset <  maxRound &&
                    <Pager.Item
                      next
                      href="#"
                      onClick={this.handleFixturesPager.bind(null,1)}>
                      &#062;
                    </Pager.Item>
                  }
                </Pager>
              </th>
            }
          </tr>
        </thead>
        <tbody>
          { (this.state.fixtures) ?
            (this.state.fixtures.fixtures.map(function(match, i) {
              var date = new Date(match.date);
              return (
                <tr key={i} style={{"fontWeight" : ((this.props.team && (match.homeTeamName==this.props.team.name
                                                                        || match.awayTeamName==this.props.team.name))?"bold":"")}}>
                  <td>{match.homeTeamName} - {match.awayTeamName}</td>
                  {(function(lang) {
                    switch(match.status) {
                        case 'FINISHED':
                            return <td>{match.result.goalsHomeTeam} : {match.result.goalsAwayTeam}</td>;
                        case 'IN_PLAY':
                            return <td><i>{match.result.goalsHomeTeam} : {match.result.goalsAwayTeam}</i></td>;
                        case 'TIMED':
                        case 'SCHEDULED':
                            return <td>{date.toLocaleDateString(lang,dateFmtOptions)}</td>;
                        default:
                            return <td></td>;
                    }
                  })(this.props.lang)}
                </tr>
              );
            }.bind(this)) )
            : <tr><td><br /><br /><br /><br /><br /><br /><div className="spinner"></div><br /></td></tr>
          }
        </tbody>
      </Table>
    );
  }

}

RoundTable.propTypes = {
  league: PropTypes.string,
  team: PropTypes.object,
  matchday: PropTypes.number,
  onError: PropTypes.func.isRequired,
  lang: PropTypes.string.isRequired,
}

module.exports = RoundTable;
