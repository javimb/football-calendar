var React = require('react');
var PropTypes = require('prop-types');
var Col = require('react-bootstrap').Col;
var Row = require('react-bootstrap').Row;
var FormGroup = require('react-bootstrap').FormGroup;
var FormControl = require('react-bootstrap').FormControl;
var ControlLabel = require('react-bootstrap').ControlLabel;
var Radio = require('react-bootstrap').Radio;
var Tabs = require('react-bootstrap').Tabs;
var Tab = require('react-bootstrap').Tab;
var Calendar = require('./Calendar');
var LeagueTable = require('./LeagueTable');
var api = require('../utils/api');
var ErrorMsg = require('./ErrorMsg');
var intl = require('react-intl-universal');
//var Select = require ('react-select'); //TODO: no funciona con require??
import Select from 'react-select';
require('react-select/dist/react-select.css');

// Import images from dir: https://stackoverflow.com/questions/42118296/dynamically-import-images-from-a-directory-using-webpack
function importAll (r) {
  let images = {};
  r.keys().map(function(key){images[key.replace('./','')]=r(key)});
  return images;
}
var flags = importAll(require.context('../../static/img/', false, /flag\.png$/));

var LEAGUES = ['PD','PL','BL1','SA','FL1'];


class SelectLeague extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      value: "-1"
    };

    this.updateValue = this.updateValue.bind(this);
  }

  updateValue(e) {
    e.persist();
    this.setState(function () {
      return {
        value: e.target.value
      }
    });
    {this.props.onSelect.call(null,e)}
  }

  render() {
    return (
      <form>
        <FormGroup>
          <ControlLabel>{intl.get('SELECT-LEAGUE')}</ControlLabel>
          { (Object.keys(this.props.leagues).length > 0) ?
            (<div onChange={this.updateValue}>
              {Object.keys(this.props.leagues).map(function(leagueKey) {
                return (
                  <Radio name="radioGroup"
                    value={this.props.leagues[leagueKey]}
                    inline
                    disabled={!this.props.leagues[leagueKey]}
                    key={leagueKey}
                    checked={this.state.value==this.props.leagues[leagueKey]}>
                    <img className="flag" src={flags[leagueKey+"flag.png"]} height="11" width="17" />
                  </Radio>
                )
              }.bind(this))}
            </div> )
            : <div><br /><div className="spinner"></div><br /></div>
          }
        </FormGroup>
      </form>
    );
  }

}

SelectLeague.propTypes = {
  leagues: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
}

function DisplayCrest (props) {
  return (
    <div>
      {!props.crestUrl ? <p></p> :
       <img className="crest" src={props.crestUrl}/>}
    </div>
  );
}

DisplayCrest.propTypes = {
  crestUrl: PropTypes.string.isRequired,
}

class SelectTeam extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      value: "-1"
    };

    this.updateValue = this.updateValue.bind(this);
  }

  updateValue(val) {
    this.setState(function () {
      return {
        value: (val)?val.value:"-1"
      }
    });
    {this.props.onSelect.call(null,(val)?val.value:null)}
  }

  render() {
    var sortedTeams = this.props.teams.sort(function(a,b) {return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0);} );
    var options = sortedTeams.map(function(team) {
                    return (
                      {value: team.code, label: team.name}
                    )
                  });

    return (
      <form>
        <FormGroup>
          <ControlLabel>{intl.get('SELECT-TEAM')}</ControlLabel>
            <Select
              name="team-select"
              onChange={this.updateValue}
              value={this.state.value}
              options={options}
              placeholder={intl.get('SELECT-TEAM')+'...'}
              noResultsText={intl.get('NO-RESULTS-FOUND')}
            />
        </FormGroup>
      </form>
    );
  }

}

SelectTeam.propTypes = {
  teams: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,
}

class Team extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      leagueCodes: {},
      myLeague: null,
      teams: [],
      myTeam: null,
      errorMsg: null
    };

    this.updateMyTeam = this.updateMyTeam.bind(this);
    this.updateMyLeague = this.updateMyLeague.bind(this);
    this.clearErrorMsg = this.clearErrorMsg.bind(this);
    this.setErrorMsg = this.setErrorMsg.bind(this);

  }

  componentDidMount () {
    this.getLeagues();
  }

  getLeagues() {
    api.fetchLeagues()
      .then(function (leagues) {
        this.setState(function () {
          var leagueCodes = {};
          LEAGUES.map(function(code){leagueCodes[code]=''});
          leagues.map(function(league) {
            if (LEAGUES.indexOf(league.league) > -1) {
              leagueCodes[league.league]=league.id;
            }
          });
          return {
            leagueCodes: leagueCodes
          }
        });
      }.bind(this))
      .catch(function (error) {
        console.log("Error: " + error);
        var errorMsg = {};
        errorMsg['title']= '' + error;
        errorMsg['text']= intl.get('ERR-LEAGUES') + intl.get('ERR-CHECK');
        this.setErrorMsg(errorMsg);
      }.bind(this));
  }

  getTeams() {
    this.setState(function() {
      return {
        teams: [],
        myTeam: null
      }
    });

    api.fetchTeams(this.state.myLeague)
      .then(function (teams) {
        this.setState(function () {
          return {
            teams: teams
          }
        });
      }.bind(this))
      .catch(function (error) {
        console.log("Error: " + error);
        var errorMsg = {};
        errorMsg['title']= '' + error;
        errorMsg['text']=intl.get('ERR-TEAMS') + intl.get('ERR-CHECK');;
        this.setErrorMsg(errorMsg);
      }.bind(this));
  }

  updateMyTeam(teamCode) {
    this.setState(function() {
      return {
        myTeam: this.state.teams.filter(function(team) {
          return team.code===teamCode;
        })[0]
      }
    });
  }

  updateMyLeague(e) {
    this.setState(function() {
      return {
        myLeague: null
      }
    });
    var leagueID = e.target.value;
    this.setState(function() {
      return {
        myLeague: leagueID
      }
    }, function() {
        this.getTeams();
    });
  }

  setErrorMsg(err) {
    this.setState(function () {
      return {
       errorMsg: err
      }
    });
  }

  clearErrorMsg() {
    this.setState(function() {
      return {
        errorMsg: null
      }
    });
  }

  render() {
    return (
      <div>
        <ErrorMsg
          msg={this.state.errorMsg}
          onDismiss={this.clearErrorMsg}
        />
        <Row className="show-grid">
          <Col xs={12} md={3}>
            <SelectLeague
              leagues={this.state.leagueCodes}
              onSelect={this.updateMyLeague}
            />
          </Col>
          <Col xs={12} md={6}>
              <SelectTeam
                teams={this.state.teams}
                onSelect={this.updateMyTeam}
              />
          </Col>
          <Col xs={6} md={3}>
            <DisplayCrest
              crestUrl={!this.state.myTeam?"":this.state.myTeam.crestUrl}
            />
          </Col>
        </Row>
        <Row className="show-grid">
          <Col xs={12}>
            <Tabs defaultActiveKey={1} id="teamTab">
              <Tab eventKey={1} title={intl.get('CALENDAR')}>
                <br />
                <Calendar
                  team={this.state.myTeam}
                  onError={this.setErrorMsg}
                  lang={this.props.lang}
                />
              </Tab>
              <Tab eventKey={2} title={intl.get('LEAGUE-TABLE')}>
                <br />
                <LeagueTable
                  league={this.state.myLeague}
                  team={this.state.myTeam}
                  onError={this.setErrorMsg}
                  lang={this.props.lang}
                />
              </Tab>
            </Tabs>
          </Col>
        </Row>
      </div>
    );
  }

}

Team.propTypes = {
  lang: PropTypes.string.isRequired,
}

module.exports = Team;
