var React = require('react');
var PropTypes = require('prop-types');
var api = require('../utils/api');
var intl = require('react-intl-universal');
var Table = require('react-bootstrap').Table;
var Col = require('react-bootstrap').Col;
var Row = require('react-bootstrap').Row;
var RoundTable = require('./RoundTable');

class LeagueTable extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      table: null
    };

  }

  componentDidMount () {
    if (this.props.league) {
      this.getLeagueTable(this.props.league);
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.league) {
      if (this.props.league!=nextProps.league) {
        this.getLeagueTable(nextProps.league);
      }
    }
    else {
      this.setState(function () {
        return {
          table: null
        }
      });
    }
  }

  getLeagueTable(leagueID) {

    this.setState(function () {
      return {
        table: null
      }
    });

    api.fetchLeagueTable(leagueID)
      .then(function (table) {
        this.setState(function () {
          return {
            table: table
          }
        });
      }.bind(this))
      .catch(function (error) {
        console.log("Error: " + error);
        var errorMsg = {};
        errorMsg['title']= '' + error;
        errorMsg['text']=intl.get('ERR-TABLE') + intl.get('ERR-CHECK');
        this.props.onError.call(null,errorMsg);
      }.bind(this));

  }

  render() {
    return (
      <div>
        <Row className="show-grid">
          <Col xs={7}>
            <Table responsive striped condensed hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th></th>
                  <th className="col-xs-5">{intl.get('TEAM')}</th>
                  <th className="col-xs-1">{intl.get('GP')}</th>
                  <th className="col-xs-1">{intl.get('WON')}</th>
                  <th className="col-xs-1">{intl.get('DRAW')}</th>
                  <th className="col-xs-1">{intl.get('LOST')}</th>
                  <th className="col-xs-1">{intl.get('GF')}</th>
                  <th className="col-xs-1">{intl.get('GA')}</th>
                  <th className="col-xs-1">{intl.get('GDIFF')}</th>
                  <th className="col-xs-1">{intl.get('POINTS')}</th>
                </tr>
              </thead>
              <tbody>
                  { this.state.table &&
                    this.state.table.standing.map(function(team, i) {
                      return (
                        <tr key={i} style={{"fontWeight" : ((this.props.team && team.teamName==this.props.team.name)?"bold":"")}}>
                          <td>{team.position}</td>
                          <td><img className="crest" src={team.crestURI}/></td>
                          <td>{team.teamName}</td>
                          <td>{team.playedGames}</td>
                          <td>{team.wins}</td>
                          <td>{team.draws}</td>
                          <td>{team.losses}</td>
                          <td>{team.goals}</td>
                          <td>{team.goalsAgainst}</td>
                          <td>{team.goalDifference}</td>
                          <td>{team.points}</td>
                        </tr>
                      );
                    }.bind(this))
                  }
              </tbody>
            </Table>
          </Col>
          <Col xs={5}>
            { this.state.table &&
              <RoundTable
                league={this.props.league}
                team={this.props.team}
                onError={this.props.onError}
                lang={this.props.lang}
                matchday={this.state.table.matchday}
              />
            }
          </Col>
        </Row>
      </div>
    );
  }

}

LeagueTable.propTypes = {
  league: PropTypes.string,
  team: PropTypes.object,
  onError: PropTypes.func.isRequired,
  lang: PropTypes.string.isRequired,
}

module.exports = LeagueTable;
