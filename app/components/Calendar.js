var React = require('react');
var PropTypes = require('prop-types');
require('react-big-calendar/lib/css/react-big-calendar.css');
import BigCalendar from 'react-big-calendar'; //TODO: no funciona con require??
var moment = require('moment');
var api = require('../utils/api');
var Match = require('./Match');
var intl = require('react-intl-universal');

BigCalendar.momentLocalizer(moment);

var MATCH_STATUS_COLOR = {
  'SCHEDULED': '#3174ad',
  'PENDING': '#ff9600',
  'WON': '#31ad4a',
  'LOST': '#ad3131',
  'DRAW': '#ffe800',
}

function MatchEvent({ event }) {
  var icon = event.atHome ? "glyphicon-home" : "glyphicon-plane";
  return (
    <div className="text-center">
      {event.rival}
      <div>
        <strong>{event.result?event.result:event.schedule}</strong>
        <span className="pull-right"><span className={"glyphicon " + icon}></span></span>
      </div>
    </div>
  )
}

class Calendar extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      events: [],
      showMatch: false,
      matchShown: null
    };

    this.openMatchModal = this.openMatchModal.bind(this);
    this.closeMatchModal = this.closeMatchModal.bind(this);

  }

  componentDidMount () {
    if (this.props.team) {
      this.getFixtures(this.props.team._links.fixtures.href);
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.team) {
      if (this.props.team!=nextProps.team) {
        this.getFixtures(nextProps.team._links.fixtures.href);
      }
    }
    else {
      this.setState(function () {
        return {
          events: []
        }
      });
    }
  }

  getFixtures(url) {

    this.setState(function () {
      return {
        events: []
      }
    });

    api.fetchFixtures(url)
      .then(function (fixtures) {
        var matches = this.getMatches(fixtures);
        this.setState(function () {
          return {
            events: matches
          }
        });
      }.bind(this))
      .catch(function (error) {
        console.log("Error: " + error);
        var errorMsg = {};
        errorMsg['title']= '' + error;
        errorMsg['text']=intl.get('ERR-FIXTURES') + intl.get('ERR-CHECK');
        this.props.onError.call(null,errorMsg);
      }.bind(this));

  }

  getMatches(fixtures) {
    return fixtures.map(function(fixture) {
        var startDate = new Date(fixture.date);
        var endDate = moment(startDate).add(105, 'm');
        var atHome = fixture.homeTeamName===this.props.team.name;
        var result = "";
        var schedule = "";
        var status = "";
        var calendar = "";
        // Possible values of the STATUS field: http://api.football-data.org/docs/v1/index.html#_fixture
        if ( fixture.status==="FINISHED" ) {
          result = fixture.result.goalsHomeTeam+" - "+fixture.result.goalsAwayTeam;
          if ( fixture.result.goalsHomeTeam===fixture.result.goalsAwayTeam ) {
            status = "DRAW";
          }
          else if ( (atHome && fixture.result.goalsHomeTeam>fixture.result.goalsAwayTeam) ||
                    (!atHome && fixture.result.goalsHomeTeam<fixture.result.goalsAwayTeam) ) {
            status = "WON";
          }
          else {
            status = "LOST";
          }
        } else {
          if ( startDate.getHours()===0 && startDate.getMinutes()===0 ) {
            status = "PENDING";
            schedule = "??:??";
          }
          else {
            status = "SCHEDULED";
            schedule = startDate.getHours()+":"+(startDate.getMinutes()<10?'0':'')+startDate.getMinutes();
            calendar = moment(startDate).utc().format('YYYYMMDDTHHmmss')+"Z/"+moment(endDate).utc().format('YYYYMMDDTHHmmss')+"Z";
          }
        }

        return {
          'title': fixture.homeTeamName + " - " + fixture.awayTeamName,
          'start': startDate,
          'end':  startDate, // dont use endDate because of duplicate rendering in calendar if match ends into next day
          'calendar': calendar,
          'rival': (atHome)?fixture.awayTeamName:fixture.homeTeamName,
          'result': result,
          'atHome': atHome,
          'status': status,
          'schedule': schedule,
          'link': fixture._links.self.href
        }
    }.bind(this));
  }

  getMatchStyle (event, start, end, isSelected) {
    //console.log(event);
    var style = {
        backgroundColor: MATCH_STATUS_COLOR[event.status],
    };
    return {
        style: style
    };
  }

  openMatchModal (event) {
    //console.log("Displaying match: "+event.title);
    //console.log(event);
    this.setState(function () {
      return {
        showMatch: true,
        matchShown: event
      }
    });
  }

  closeMatchModal() {
    this.setState(function () {
      return {
        showMatch: false,
        matchShown: null
      }
    });
  }

  render() {
    return(
      <div className="calendar">
        <BigCalendar
          events={this.state.events}
          views={['month']}
          components={{event: MatchEvent}}
          eventPropGetter={(this.getMatchStyle)}
          onSelectEvent={this.openMatchModal}
          culture={this.props.lang}
          messages={{
            today: intl.get('TODAY'),
            next: '>',
            previous: '<'
          }}
        />
        <Match
          match={this.state.matchShown}
          show={this.state.showMatch}
          onError={this.props.onError}
          onClose={this.closeMatchModal}
          lang={this.props.lang}
        />
      </div>
    );
  }
}

Calendar.propTypes = {
  team: PropTypes.object,
  onError: PropTypes.func.isRequired,
  lang: PropTypes.string.isRequired,
}

module.exports = Calendar;
