var axios = require('axios');

module.exports = {

  fetchLeagues: function () {
    var encodedURI = window.encodeURI('/api/competitions');
    return axios.get(encodedURI)
      .then(function (response) {
        console.log(response);
        return response.data;
      });

  },

  fetchTeams: function (league) {
    var encodedURI = window.encodeURI('/api/competitions/'+ league + '/teams' );
    return axios.get(encodedURI)
      .then(function (response) {
        console.log(response);
        return response.data;
      });
  },

  fetchFixtures: function (url) {
    var tokens = url.split("/");
    var teamID = tokens[tokens.length-2];
    //console.log("Team ID: "+teamID);
    var encodedURI = window.encodeURI('/api/teams/'+ teamID +'/fixtures');
    return axios.get(encodedURI)
      .then(function (response) {
        console.log(response);
        return response.data;
      });
  },

  fecthMatch: function (url) {
    var tokens = url.split("/");
    var matchID = tokens[tokens.length-1];
    var encodedURI = window.encodeURI('/api/fixtures/'+ matchID);
    return axios.get(encodedURI)
      .then(function (response) {
        console.log(response);
        return response.data;
      });
  },

  fetchLeagueTable: function (league) {
      var encodedURI = window.encodeURI('/api/competitions/'+ league + '/leagueTable' );
      return axios.get(encodedURI)
        .then(function (response) {
          console.log(response);
          return response.data;
        });
  },

  fetchRoundFixtures: function (league,round) {
    var encodedURI = window.encodeURI('/api/competitions/'+ league + '/fixtures?matchday='+round );
    return axios.get(encodedURI)
      .then(function (response) {
        console.log(response);
        return response.data;
      });
  }

}
